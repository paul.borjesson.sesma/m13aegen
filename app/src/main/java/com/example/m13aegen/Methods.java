package com.example.m13aegen;


import android.util.Log;

import androidx.annotation.NonNull;

import com.example.m13aegen.model.Match;
import com.example.m13aegen.model.PublicUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class Methods {
    interface CheckAvailabilityCallback {
        void onCheckFinished(boolean available);
    }
    public static void checkAvailability(FirebaseFirestore db, String username, CheckAvailabilityCallback callback) {
        db.collection("users").whereEqualTo("userName", username).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if (task.getResult().size() > 0) {
                    callback.onCheckFinished(false);
                } else {
                    callback.onCheckFinished(true);
                }
            } else {
                Log.d("TAG", "Error getting documents: ", task.getException());
                callback.onCheckFinished(false);
            }
        });
    }

    interface CheckAvailabilityGoogleAuthCallback {
        void onCheckFinishedGoogle(String finalUsername);
    }

    public static void checkAvailabilityGoogleAuth(FirebaseFirestore db, String username, CheckAvailabilityGoogleAuthCallback callback) {
        db.collection("users").whereEqualTo("userName", username).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if (task.getResult().size() > 0) {
                    callback.onCheckFinishedGoogle(username + task.getResult().size());
                } else {
                    callback.onCheckFinishedGoogle(username);
                }
            } else {
                Log.d("TAG", "Error getting documents: ", task.getException());
                callback.onCheckFinishedGoogle(username);
            }
        });
    }

    public static String calcWinrate(int total, int won) {
        if (total != 0) {
            return won * 100 / total + "%";
        }
        return "0%";
    }

    interface computePointsCallback {
        void onCheckFinished(int oldPointsP1, int newPointsP1, int oldPointsP2, int newPointsP2);
    }

    public static void computePoints(FirebaseFirestore db, Match match, boolean isPlayerOneWinner, computePointsCallback callback) {
        int K = 24;
        db.collection("users").document(match.player1).get().addOnSuccessListener(docP1 -> {
            db.collection("users").document(match.player2).get().addOnSuccessListener(docP2 -> {
                PublicUser player1 = docP1.toObject(PublicUser.class);
                PublicUser player2 = docP2.toObject(PublicUser.class);
                int points1 = 0;
                int points2 = 0;
                if (player1 != null && player2 != null) {
                    int diffRating = Math.abs(player1.points - player2.points);
                    double val = Math.pow(10, diffRating / 400) + 1;
                    double varianceCoefficient = 1 / val;
                    if (isPlayerOneWinner) {
                        points1 = (int) (player1.points + K * (1 - varianceCoefficient));
                        points2 = (int) (player2.points + K * (-varianceCoefficient));
                    } else {
                        points1 = (int) (player1.points + K * (-varianceCoefficient));
                        points2 = (int) (player2.points + K * (1 - varianceCoefficient));
                    }
                }
                callback.onCheckFinished(player1.points, points1, player2.points, points2);
            });
        });
    }
}
