package com.example.m13aegen;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.m13aegen.databinding.CeldaBinding;
import com.example.m13aegen.databinding.FragmentTresEnRayaBinding;
import com.example.m13aegen.model.Cell;
import com.example.m13aegen.model.Match;
import com.example.m13aegen.model.PublicUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.ArrayList;
import java.util.List;

public class TresEnRayaFragment extends AppFragment {

    private FragmentTresEnRayaBinding binding;
    private String matchId;
    private TresEnRayaAdapter adapter;
    private List<Cell> cellList = new ArrayList<>();
    private Match match;
    private ListenerRegistration listener = null;
    boolean gameFinished;
    boolean uploaded;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return  (binding = FragmentTresEnRayaBinding.inflate(inflater)).getRoot();
    }

    @Override
    public void onStop() {
        if (listener != null) {
            listener.remove();
        }
        super.onStop();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Receive matchId
        matchId = this.getArguments().getString("key");

        //Initialize match
        initMatch();

        //Initialize listener
        listenerStance();

        binding.table.setAdapter(adapter = new TresEnRayaAdapter());
        binding.table.setLayoutManager(new GridLayoutManager(getContext(), 3));

        for (int i = 0; i < 9; i++) {
            cellList.add(new Cell());
        }

        adapter.setOnCellClickedListener((cell, position) -> {
            if (cell.player == 0) {
                if (auth.getCurrentUser().getUid().equals(match.player1)) {             //Player 1
                    if (match.moves.size() % 2 == 0) {
                        match.moves.add(position);
                    }
                } else if (auth.getCurrentUser().getUid().equals(match.player2)) {      //Player2
                    if (match.moves.size() % 2 == 1) {
                        match.moves.add(position);
                    }
                }
                setDocRef().set(match);
            }
        });

    }

    private void initMatch() {
        setDocRef().get().addOnSuccessListener(documentSnapshot -> match = documentSnapshot.toObject(Match.class));
        gameFinished = false;
        uploaded = false;
    }

    private void listenerStance() {         //Constantly updates match and the visual state of the game
        listener = setDocRef().addSnapshotListener((value, error) -> {
            if (value.exists()) {
                if (error != null) {
                    Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                //String source = value != null && value.getMetadata().hasPendingWrites() ? "Local" : "Server";
                //if (value.exists() && source.equals("Server")) {
                match = value.toObject(Match.class);
                for (int i = 0; i < match.moves.size(); i++) {
                    cellList.get(match.moves.get(i)).player = i % 2 == 0 ? 1 : 4;
                    //cellList.set(match.moves.get(i), new Cell(i % 2 == 0 ? 1 : -1));
                }
                adapter.notifyDataSetChanged();

                gameState(cellList, match);
            }
        });
    }

    void gameState(List<Cell> cellList, Match match) {
        int value;
        String print = "";
        for (int i = 0; i < 9; i++) {
            if (i == 3 || i == 6) {
                print += "\n";
            }
            print += "[" + cellList.get(i).player + "]";
        }
        System.out.println(print);
        //filas
        for (int i = 0; i < 9; i += 3) {            //0, 3, 6
            value = 0;
            for (int j = 0; j < 3; j++) {           //0, 1, 2
                value += cellList.get(j + i).player;        //0, 1, 2   -->    3, 4, 5 ...
            }
            if (winner(value, match)) return;
        }
        //columnas
        for (int i = 0; i < 3; i++) {               //0, 1, 2
            value = 0;
            for (int j = 0; j < 9; j += 3) {           //0, 3, 6
                value += cellList.get(i + j).player;        //0, 3, 6  -->    1, 4, 7 ...
            }
            if (winner(value, match)) return;
        }
        //diagonal
        value = 0;
        for (int i = 0; i <= 8; i += 4) {
            value += cellList.get(i).player;
        }
        if (winner(value, match)) return;
        //diagonal inversa
        value = 0;
        for (int i = 2; i <= 6; i += 2) {
            value += cellList.get(i).player;
        }
        if (winner(value, match)) return;

        //draw?
        value = 0;
        for (Cell c : cellList) {
            if (c.player == 0) value++;
        }
        if (value == 0 && !uploaded) {
            //draw
            draw(match);
        } else {
            binding.turno.setText("Turn of " + (match.moves.size() % 2 == 0 ? "X" : "O"));
        }
    }

    boolean winner(int value, Match match) {
        if (value != 3 && value != 12) {
            return false;
        } else {
            gameFinished = true;
            Methods.computePoints(db, match, value == 3, (oldPointsP1, newPointsP1, oldPointsP2, newPointsP2) -> {
                //Hay que limitar a actualizar solamente el tuyo, porque en caso contrario se haría un update en la base de datos por ambas partes, o sea dos
                if (auth.getCurrentUser().getUid().equals(match.player1) && !uploaded) {
                    //ANIMATION
                    setUserRef(match.player1).get().addOnSuccessListener(documentSnapshot -> {
                        animationWinLose(oldPointsP1, newPointsP1);
                        uploadPoints(match.player1, newPointsP1, value, 3);
                        uploaded = true;
                    });
                } else if (!uploaded) {
                    //ANIMATION
                    setUserRef(match.player2).get().addOnSuccessListener(documentSnapshot -> {
                        animationWinLose(oldPointsP2, newPointsP2);
                        uploadPoints(match.player2, newPointsP2, value, 12);
                        uploaded = true;
                    });
                }
                setDocRef().update("winnerId", value == 3 ? match.player1 : match.player2);
            });
        }
        uploaded = false;
        return true;
    }

    DocumentReference setUserRef(String doc) { return db.collection("users").document(doc); }

    void draw(Match match) {
        System.out.println("ENTERED DRAW");
        gameFinished = true;
        if (auth.getCurrentUser().getUid().equals(match.player1)) {
            uploadPointsOnDraw(match.player1);
        } else {
            //update points2
            uploadPointsOnDraw(match.player2);
        }
        uploaded = true;
        setDocRef().update("winnerId", "Draw");
    }

    void uploadPoints(String player, int newPointsP, int value, int expected) {
        db.collection("users").document(player)
                .update("points", newPointsP,
                        "gamesPlayed", FieldValue.increment(1),
                        "gamesWon", value == expected ? FieldValue.increment(1) : FieldValue.increment(0));
    }

    void uploadPointsOnDraw(String player) {
        db.collection("users").document(player)
                .update("gamesPlayed", FieldValue.increment(1));
        db.collection("users").document(player).get().addOnCompleteListener(task -> {
            animationWinLose(task.getResult().toObject(PublicUser.class).points, task.getResult().toObject(PublicUser.class).points);
        });
    }

    private void animationWinLose(int oldVal, int newVal) {
        binding.pointsCounter.setVisibility(View.VISIBLE);
        binding.pointsCounter.setText(oldVal + "");
        if (oldVal > newVal) {
            binding.pointsCounter.setTextColor(getResources().getColor(R.color.red1));
        } else if (oldVal < newVal) {
            binding.pointsCounter.setTextColor(getResources().getColor(R.color.green1));
        } else {
            binding.pointsCounter.setTextColor(getResources().getColor(R.color.golden));
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        float height = -displayMetrics.heightPixels/2;

        AnimatorSet set = new AnimatorSet();
        AnimatorSet subSet = new AnimatorSet();

        ObjectAnimator transYDown = ObjectAnimator.ofFloat(binding.pointsCounter, "translationY", height, 0f).setDuration(1500);
        ObjectAnimator transYUp = ObjectAnimator.ofFloat(binding.pointsCounter, "translationY", 0f, height).setDuration(1500);
        transYUp.setStartDelay(1000);
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(binding.pointsCounter, "alpha", 1f, 0f);

        ValueAnimator counterAnimator = ValueAnimator.ofInt(oldVal, newVal);
        counterAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        counterAnimator.setDuration(2000);
        counterAnimator.addUpdateListener(animation -> {
            Integer value = (Integer) animation.getAnimatedValue();
            binding.pointsCounter.setText(value + "");
        });

        int colorFrom;
        int colorTo;
        if (oldVal > newVal) {
            colorFrom = getResources().getColor(R.color.red1);
            colorTo = getResources().getColor(R.color.red2);
        } else if (oldVal < newVal) {
            colorFrom = getResources().getColor(R.color.green1);
            colorTo = getResources().getColor(R.color.green2);
        } else {
            colorFrom = getResources().getColor(R.color.golden);
            colorTo = getResources().getColor(R.color.golden);
        }
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(2000); // milliseconds
        colorAnimation.addUpdateListener(animator -> binding.pointsCounter.setTextColor((int) animator.getAnimatedValue()));

        subSet.playTogether(counterAnimator, colorAnimation);
        set.playSequentially(transYDown, subSet, transYUp, fadeOut);
        set.start();

    }

    DocumentReference setDocRef() { return db.collection("matches").document(matchId); }

    class TresEnRayaAdapter extends RecyclerView.Adapter<TresEnRayaAdapter.MyViewHolder> {
        private int imgX;
        private int imgO;
        private OnCellClickedListener onCellClickedListener;

        public TresEnRayaAdapter() {
            imgX = R.drawable.ic_cross;
            imgO = R.drawable.ic_circle;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(CeldaBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            Cell cell = cellList.get(position);

            if (cell.player == 1) {
                holder.binding.cellRes.setImageResource(imgX);
            } else if (cell.player == 4){
                holder.binding.cellRes.setImageResource(imgO);
            } else {
                holder.binding.cellRes.setImageResource(0);
            }

            if (!gameFinished) {
                holder.binding.cellRes.setOnClickListener(view -> {
                    if (onCellClickedListener != null) {
                        onCellClickedListener.onCellClicked(cell, position);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return cellList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CeldaBinding binding;
            public MyViewHolder(CeldaBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }

        void setOnCellClickedListener(OnCellClickedListener listener) {
            this.onCellClickedListener = listener;
        }
    }

    interface OnCellClickedListener {
        void onCellClicked(Cell cell, int position);
    }

}