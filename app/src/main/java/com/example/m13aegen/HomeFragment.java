package com.example.m13aegen;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.m13aegen.databinding.FragmentHomeBinding;
import com.example.m13aegen.databinding.MyMatchRowBinding;
import com.example.m13aegen.model.Match;
import com.google.android.gms.common.api.Batch;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class HomeFragment extends AppFragment {

    private FragmentHomeBinding binding;
    private MatchAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return (binding = FragmentHomeBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.gamesRV.setAdapter(adapter = new MatchAdapter(this));
        binding.gamesRV.setLayoutManager(new LinearLayoutManager(getContext()));

        binding.txtLoading.setVisibility(View.GONE);
        binding.relContent.setVisibility(View.VISIBLE);

        binding.btnCreate.setOnClickListener(view1 -> {
            Match matchNew = new Match();
            matchNew.player1 = auth.getCurrentUser().getUid();
            matchNew.player1name = auth.getCurrentUser().getDisplayName();
            matchNew.matchId = db.collection("matches").document().getId();
            matchNew.moves = new ArrayList<>();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                matchNew.startTime = LocalDateTime.now().toString();
            }

            //delete any other ongoing match from that user
            //whereEqualTo a null field (winnerId) doesn't work properly, so I'll do it manually
            db.collection("matches").whereEqualTo("player1", auth.getCurrentUser().getUid()).get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    List<Match> matchList = task.getResult().toObjects(Match.class);
                    for (int i = 0; i < matchList.size(); i++) {
                        if (matchList.get(i).winnerId == null || "".equals(matchList.get(i).winnerId)) {      //unfinished match
                            db.collection("matches").document(task.getResult().getDocuments().get(i).getId()).delete();
                        }
                    }
                    setMatchAndFragment(matchNew);
                } else {
                    setMatchAndFragment(matchNew);
                }
            });
        });

        db.collection("matches").orderBy("startTime", Query.Direction.DESCENDING).addSnapshotListener((collectionSnapshot, e) -> {
            List<Match> newMatchList = new ArrayList<>();
            if (collectionSnapshot != null) {
                for (DocumentSnapshot documentSnapshot : collectionSnapshot) {
                    Match match = documentSnapshot.toObject(Match.class);
                    if (match != null) {
                        if (match.winnerId == null || "".equals(match.winnerId)) {
                            match.matchId = documentSnapshot.getId();
                            newMatchList.add(match);
                        }
                    }
                }
            }
            adapter.updateMatchList(newMatchList);
            //binding.gamesRV.scrollToPosition(0);
        });
    }

    private void setMatchAndFragment(Match matchNew) {
        db.collection("matches").document(matchNew.matchId)
                .set(matchNew);

        db.collection("matches").document(matchNew.matchId).addSnapshotListener((value, error) -> {
            Match match = value.toObject(Match.class);
            binding.txtLoading.setVisibility(View.VISIBLE);
            binding.relContent.setVisibility(View.GONE);
            if (match != null) {
                if(match.player2 != null) {
                    setFragment(new TresEnRayaFragment(), match.matchId);
                }
            }
        });
    }

    private void setFragment(Fragment fragment, String key) {
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        fragment.setArguments(bundle);
        if (this.getFragmentManager() != null)
            this.getFragmentManager().beginTransaction().replace(R.id.mainFrame, fragment).commit();
    }

}