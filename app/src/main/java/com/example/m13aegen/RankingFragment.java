package com.example.m13aegen;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.m13aegen.databinding.ActivityMainBinding;
import com.example.m13aegen.databinding.FragmentRankingBinding;
import com.example.m13aegen.databinding.MyRankRowBinding;
import com.example.m13aegen.model.Match;
import com.example.m13aegen.model.PublicUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RankingFragment extends AppFragment {

    private FragmentRankingBinding binding;
    private RankAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return (binding = FragmentRankingBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recRanks.setAdapter(adapter = new RankAdapter());
        binding.recRanks.setLayoutManager(new LinearLayoutManager(getContext()));

        db.collection("users").orderBy("points", Query.Direction.DESCENDING).addSnapshotListener((collectionSnapshot, e) -> {
            List<PublicUser> newRankList = new ArrayList<>();
            if (collectionSnapshot != null) {
                for (DocumentSnapshot documentSnapshot : collectionSnapshot) {
                    PublicUser pUser = documentSnapshot.toObject(PublicUser.class);
                    if (pUser != null) {
                        newRankList.add(pUser);
                    }
                }
            }
            adapter.updateRankList(newRankList);
        });

        binding.btnSelf.setOnClickListener(view1 -> {
            db.collection("users").orderBy("points", Query.Direction.DESCENDING).get().addOnSuccessListener(queryDocumentSnapshots -> {
                List<PublicUser> publicUsers = new ArrayList<>();
                for (QueryDocumentSnapshot q : queryDocumentSnapshots) {
                    publicUsers.add(q.toObject(PublicUser.class));
                    if (publicUsers.get(publicUsers.size() - 1).userName.equals(auth.getCurrentUser().getDisplayName())) {
                        binding.recRanks.scrollToPosition(publicUsers.size() - 1);
                    }
                }
            });
        });

        //FIX

    }

    class RankAdapter extends RecyclerView.Adapter<RankAdapter.ViewHolder> {
        private final List<PublicUser> rankList = new ArrayList<>();

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(MyRankRowBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            PublicUser rank = rankList.get(position);
            if (position == 0) {
                holder.binding.crownImg.setImageResource(R.drawable.goldencrown);
            } else if (position == 1) {
                holder.binding.crownImg.setImageResource(R.drawable.silvercrown);
            } else if (position == 2) {
                holder.binding.crownImg.setImageResource(R.drawable.bronzecrown);
            } else {
                holder.binding.crownImg.setImageResource(0);
            }

            if (rank.userName.equals(auth.getCurrentUser().getDisplayName())) {
                holder.binding.card.setStrokeWidth(10);
            } else {
                holder.binding.card.setStrokeWidth(0);
            }
            holder.binding.usernameField.setText(rank.userName);
            holder.binding.textRank.setText("#" + (position + 1));
            holder.binding.totalPointsVal.setText(rank.points + "");
            holder.binding.winrateTxtVal.setText(Methods.calcWinrate(rank.gamesPlayed, rank.gamesWon));

            holder.binding.relativeLayoutRank.setOnClickListener(view -> {
                if (view.getContext() != null) {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    ProfileFragment pF = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("uName", rank.userName);
                    pF.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame, pF).commit();
                 }
            });
        }

        @Override
        public int getItemCount() {
            return rankList.size();
        }

        public void updateRankList(List<PublicUser> newList) {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return rankList.size();
                }

                @Override
                public int getNewListSize() {
                    return newList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return rankList.get(oldItemPosition).userName.equals(newList.get(newItemPosition).userName);
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    PublicUser oldRank = rankList.get(oldItemPosition);
                    PublicUser newRank = newList.get(newItemPosition);
                    return !Objects.equals(oldRank.userName, newRank.userName);
                }
            });

            rankList.clear();
            rankList.addAll(newList);
            diffResult.dispatchUpdatesTo(this);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            MyRankRowBinding binding;
            public ViewHolder(MyRankRowBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}