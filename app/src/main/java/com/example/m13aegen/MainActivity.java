package com.example.m13aegen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.m13aegen.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private RankingFragment rankingFragment;
    private HomeFragment homeFragment;
    private ProfileFragment profileFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        boolean isNightMode = getSharedPreferences("AppSettingsPrefs", 0).getBoolean("NightMode", false);

        if (isNightMode) {
            setTheme(R.style.Theme_Dark);
        } else {
            setTheme(R.style.Theme_Light);
        }

        rankingFragment = new RankingFragment();
        homeFragment = new HomeFragment();
        profileFragment = new ProfileFragment();

        setFragment(homeFragment);

        binding.mainNav.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nav_home:
                    //home fragment
                    setFragment(homeFragment);
                    return true;
                case R.id.nav_record:
                    //records fragment
                    setFragment(rankingFragment);
                    return true;
                case R.id.nav_profile:
                    //profile fragment
                    setFragment(profileFragment);
                    return true;
                default:
                    return false;
            }
        });

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.commit();
    }
}