package com.example.m13aegen.model;
import java.util.List;

public class Match {
    public String player1;
    public String player1name;
    public String player2;
    public String player2name;
    public String matchId;
    public String startTime;
    public List<Integer> moves;
    public String winnerId;

    @Override
    public String toString() {
        return "Match{" +
                "player1='" + player1 + '\'' +
                ", player1name='" + player1name + '\'' +
                ", player2='" + player2 + '\'' +
                ", player2name='" + player2name + '\'' +
                ", matchId='" + matchId + '\'' +
                ", startTime='" + startTime + '\'' +
                ", moves=" + moves +
                ", winnerId='" + winnerId + '\'' +
                '}';
    }
}
