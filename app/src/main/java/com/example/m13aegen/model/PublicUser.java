package com.example.m13aegen.model;

public class PublicUser {
    public String userName;
    public int gamesWon;
    public int gamesPlayed;
    public int points;

    @Override
    public String toString() {
        return "PublicUser{" +
                "userName='" + userName + '\'' +
                ", gamesWon=" + gamesWon +
                ", gamesPlayed=" + gamesPlayed +
                ", points=" + points +
                '}';
    }
}
