package com.example.m13aegen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.m13aegen.model.Match;
import com.example.m13aegen.model.PublicUser;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends AppFragment {

    private com.example.m13aegen.databinding.FragmentProfileBinding binding;
    private boolean firstTime = true;
    private MatchAdapter adapter;
    private String currentUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return (binding = com.example.m13aegen.databinding.FragmentProfileBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            binding.toggleMode.setImageResource(R.drawable.ic_light_mode);
            binding.btnLogout.setImageResource(R.drawable.ic_logout);
            binding.search.setImageResource(R.drawable.ic_search);
        } else {
            binding.toggleMode.setImageResource(R.drawable.ic_dark_mode);
            binding.btnLogout.setImageResource(R.drawable.ic_logout2);
            binding.search.setImageResource(R.drawable.ic_search2);
        }

        binding.username.setText(auth.getCurrentUser().getDisplayName());
        binding.email.setText(auth.getCurrentUser().getEmail());
        binding.historyTxt.setVisibility(View.INVISIBLE);
        binding.historyValW.setVisibility(View.INVISIBLE);
        binding.historyValL.setVisibility(View.INVISIBLE);
        currentUser = auth.getCurrentUser().getDisplayName();

        db.collection("users").document(auth.getCurrentUser().getUid()).get().addOnSuccessListener(documentSnapshot -> {
            PublicUser localUser = documentSnapshot.toObject(PublicUser.class);
            if (localUser != null) {
                binding.gamesPlayedValue.setText(localUser.gamesPlayed + "");
                binding.gamesWonValue.setText(localUser.gamesWon + "");
                binding.winrateValue.setText(Methods.calcWinrate(localUser.gamesPlayed, localUser.gamesWon));
            }
        });

        rankWins(auth.getCurrentUser().getDisplayName());
        rankPoints(auth.getCurrentUser().getDisplayName());

        if (this.getArguments() != null && firstTime) {
            String uNameFromRank = this.getArguments().getString("uName");
            if (uNameFromRank != null || !uNameFromRank.isEmpty()) {
                binding.searchBar.setText(uNameFromRank);
                search(view, savedInstanceState);
            }
            firstTime = false;
        }

        //binding.recMat.setAdapter(adapter = new MatchAdapter(this));
        //binding.recMat.setLayoutManager(new LinearLayoutManager(getContext()));

        binding.searchBar.setOnKeyListener((view12, i, keyEvent) -> {
            if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_ENTER) {
                search(view, savedInstanceState);
                return true;
            }
            return false;
        });

        binding.search.setOnClickListener(view13 -> {
            search(view, savedInstanceState);
        });

        binding.btnLogout.setOnClickListener(view1 -> {
            auth.signOut();
            GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build());
            mGoogleSignInClient.signOut();
            startActivity(new Intent(requireContext(), LoginActivity.class));
        });

        binding.toggleMode.setOnClickListener(view14 -> {
            if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                getActivity().getSharedPreferences("AppSettingsPrefs", 0).edit().putBoolean("NightMode", false).apply();
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                getActivity().getSharedPreferences("AppSettingsPrefs", 0).edit().putBoolean("NightMode", true).apply();
            }
        });

        //summon();

    }

    void summon() {
        db.collection("matches").orderBy("startTime", Query.Direction.DESCENDING).addSnapshotListener((collectionSnapshot, e) -> {
            List<Match> newMatchList = new ArrayList<>();
            if (collectionSnapshot != null) {
                for (DocumentSnapshot documentSnapshot : collectionSnapshot) {
                    Match match = documentSnapshot.toObject(Match.class);
                    if (match != null) {
                        System.out.println(currentUser + " cu");
                        System.out.println(match.toString());
                        if (match.player1name.equals(currentUser) || currentUser.equals(match.player2name)) {
                            match.matchId = documentSnapshot.getId();
                            newMatchList.add(match);
                        }
                    }
                }
            }

            adapter.updateMatchList(newMatchList);
            //binding.gamesRV.scrollToPosition(0);
        });
    }

    void search(View view, Bundle savedInstanceState) {
        if (binding.searchBar.getText().toString().isEmpty()) {
            onViewCreated(view, savedInstanceState);
        } else {
            db.collection("users").whereEqualTo("userName", binding.searchBar.getText().toString()).limit(1).get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    List<PublicUser> users = task.getResult().toObjects(PublicUser.class);
                    if (!users.isEmpty()) { //only 1 result available since limit is set to one
                        binding.username.setText(users.get(0).userName);
                        binding.email.setText("");
                        binding.gamesPlayedValue.setText(users.get(0).gamesPlayed + "");
                        binding.gamesWonValue.setText(users.get(0).gamesWon + "");
                        binding.winrateValue.setText(Methods.calcWinrate(users.get(0).gamesPlayed, users.get(0).gamesWon));

                        rankWins(users.get(0).userName);
                        rankPoints(users.get(0).userName);

                        binding.historyTxt.setVisibility(View.VISIBLE);
                        binding.historyValW.setVisibility(View.VISIBLE);
                        binding.historyValL.setVisibility(View.VISIBLE);
                        personWinLoses(users.get(0).userName);

                        currentUser = users.get(0).userName;
                        //summon();

                    } else {
                        Toast.makeText(getContext(), "The user " + binding.searchBar.getText().toString() + " couldn't be found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Error: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    void rankWins(String displayName) {
        db.collection("users").orderBy("gamesWon", Query.Direction.DESCENDING).get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<PublicUser> publicUsers = new ArrayList<>();
            for (QueryDocumentSnapshot q : queryDocumentSnapshots) {
                publicUsers.add(q.toObject(PublicUser.class));
                if (publicUsers.get(publicUsers.size() - 1).userName.equals(displayName)) break;
            }
            binding.worldRankWinsVal.setText("#" + publicUsers.size());
        });
    }

    void rankPoints(String displayName) {
        db.collection("users").orderBy("points", Query.Direction.DESCENDING).get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<PublicUser> publicUsers = new ArrayList<>();
            for (QueryDocumentSnapshot q : queryDocumentSnapshots) {
                publicUsers.add(q.toObject(PublicUser.class));
                if (publicUsers.get(publicUsers.size() - 1).userName.equals(displayName)) break;
            }
            binding.worldRankPointsVal.setText("#" + publicUsers.size());
        });
    }

    void personWinLoses(String playerFromProfile) {
        binding.historyValW.setText("0");
        binding.historyValL.setText("0");
        db.collection("users").whereEqualTo("userName", playerFromProfile).limit(1).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot p : task.getResult()) {
                    String doc = p.getId();
                    if (doc != null && !"".equals(doc)) {
                        db.collection("matches").whereEqualTo("player1", auth.getCurrentUser().getUid()).whereEqualTo("player2", doc).get().addOnSuccessListener(queryDocumentSnapshots -> {
                            for (Match m : queryDocumentSnapshots.toObjects(Match.class)) {
                                if (m.winnerId != null && !"Draw".equals(m.winnerId)) {
                                    if (m.winnerId.equals(m.player1)) {
                                        sumWinsLoses(binding.historyValW);
                                    } else {
                                        sumWinsLoses(binding.historyValL);
                                    }
                                }
                            }
                            db.collection("matches").whereEqualTo("player1", doc).whereEqualTo("player2", auth.getCurrentUser().getUid()).get().addOnSuccessListener(queryDocumentSnapshots1 -> {
                                for (Match m : queryDocumentSnapshots1.toObjects(Match.class)) {
                                    if (m.winnerId != null && !"Draw".equals(m.winnerId)) {
                                        if (m.winnerId.equals(m.player1)) {
                                            sumWinsLoses(binding.historyValL);
                                        } else {
                                            sumWinsLoses(binding.historyValW);
                                        }
                                    }
                                }
                                binding.historyValW.setText(binding.historyValW.getText() + "W");
                                binding.historyValL.setText(binding.historyValL.getText() + "L");
                            });
                        });
                    }
                }
            }
        });
    }

    void sumWinsLoses(TextView view) {
        view.setText(Integer.parseInt((String) view.getText()) + 1 + "");
    }


}