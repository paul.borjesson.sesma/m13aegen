package com.example.m13aegen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.example.m13aegen.databinding.ActivitySignupBinding;
import com.example.m13aegen.model.PublicUser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;

public class SignupActivity extends AppCompatActivity {

    private ActivitySignupBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignupBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnSignup2.setOnClickListener(v -> {
            if (binding.tilUsername.getEditText().getText().toString().isEmpty()) {
                binding.tilUsername.getEditText().setError("Required username.");
                return;
            }
            if (binding.tilEmail.getEditText().getText().toString().isEmpty()) {
                binding.tilEmail.getEditText().setError("Required email.");
                return;
            }
            if (binding.tilPass.getEditText().getText().toString().isEmpty()) {
                binding.tilPass.getEditText().setError("Required password.");
                return;
            }

            FirebaseAuth auth = FirebaseAuth.getInstance();

            Methods.checkAvailability(FirebaseFirestore.getInstance(), binding.tilUsername.getEditText().getText().toString(), available -> {
                if (!available) {
                    Toast.makeText(getApplicationContext(), "The username is already on use", Toast.LENGTH_LONG).show();
                    binding.tilUsername.getEditText().setHighlightColor(getResources().getColor(R.color.avaRed));
                } else {
                    auth.createUserWithEmailAndPassword(
                            binding.tilEmail.getEditText().getText().toString(),
                            binding.tilPass.getEditText().getText().toString()
                    ).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            FirebaseUser user = auth.getCurrentUser();
                            if (user != null) {
                                UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(binding.tilUsername.getEditText().getText().toString())
                                        .build();
                                user.updateProfile(profileUpdate);
                                PublicUser publicUser = new PublicUser();
                                publicUser.userName = binding.tilUsername.getEditText().getText().toString();
                                publicUser.gamesPlayed = 0;
                                publicUser.gamesWon = 0;
                                publicUser.points = 1000;

                                FirebaseFirestore.getInstance().collection("users").document(auth.getCurrentUser().getUid()).set(publicUser);
                                startActivity(new Intent(SignupActivity.this, MainActivity.class));
                            }
                        } else {
                            Log.d("FAIL", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(this, task.getException().getLocalizedMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
        });

    }
}