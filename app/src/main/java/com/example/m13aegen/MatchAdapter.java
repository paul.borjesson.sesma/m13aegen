package com.example.m13aegen;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.m13aegen.databinding.MyMatchRowBinding;
import com.example.m13aegen.model.Match;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.ViewHolder> {
    private final List<Match> matchList = new ArrayList<>();
    Fragment fragment;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseAuth auth = FirebaseAuth.getInstance();

    public MatchAdapter(Fragment fragment) {
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(MyMatchRowBinding.inflate(fragment.getLayoutInflater(), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Match match = matchList.get(position);
        holder.binding.matchName.setText(match.player1name);
        holder.binding.matchDate.setText(match.startTime.substring(11, 16)); //0,16

        if (match.player2 == null || "".equals(match.player2)) {
            holder.binding.matchPlayer.setText("");
            holder.binding.card.setStrokeColor(fragment.getResources().getColor(R.color.avaGreen));
            holder.binding.rel.setOnClickListener(view -> {
                if (!match.player1.equals(auth.getCurrentUser().getUid())) {
                    db.collection("matches").document(match.matchId).update("player2", auth.getCurrentUser().getUid());
                    db.collection("matches").document(match.matchId).update("player2name", auth.getCurrentUser().getDisplayName());
                    //
                    if (view.getContext() != null) {
                        AppCompatActivity activity = (AppCompatActivity) view.getContext();
                        TresEnRayaFragment r3F = new TresEnRayaFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("key", match.matchId);
                        r3F.setArguments(bundle);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame, r3F).commit();
                    }
                } else {
                    Toast.makeText(fragment.getContext(), "You cannot join a game with yourself", Toast.LENGTH_SHORT);
                }
            });
        } else {
            holder.binding.matchPlayer.setText(match.player2name);
            holder.binding.card.setStrokeColor(fragment.getResources().getColor(R.color.avaRed));
            holder.binding.moves.setVisibility(View.VISIBLE);
            holder.binding.numTurns.setVisibility(View.VISIBLE);
            holder.binding.numTurns.setText(match.moves.size() + "");

            holder.binding.rel.setOnClickListener(view -> {
                if (view.getContext() != null) {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    TresEnRayaFragment r3F = new TresEnRayaFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("key", match.matchId);
                    r3F.setArguments(bundle);
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame, r3F).commit();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return matchList.size();
    }

    public void updateMatchList(List<Match> newList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return matchList.size();
            }

            @Override
            public int getNewListSize() {
                return newList.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return matchList.get(oldItemPosition).matchId.equals(newList.get(newItemPosition).matchId);
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                Match oldMatch = matchList.get(oldItemPosition);
                Match newMatch = newList.get(newItemPosition);
                if (!Objects.equals(oldMatch.matchId, newMatch.matchId)) return false;
                return (!Objects.equals(oldMatch.startTime, newMatch.startTime));
            }
        });

        matchList.clear();
        matchList.addAll(newList);
        diffResult.dispatchUpdatesTo(this);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MyMatchRowBinding binding;
        public ViewHolder(MyMatchRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
