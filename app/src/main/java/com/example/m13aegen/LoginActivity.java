package com.example.m13aegen;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.example.m13aegen.databinding.ActivityLoginBinding;
import com.example.m13aegen.model.PublicUser;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.signInProgressBar.setVisibility(View.GONE);

        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(getApplicationContext(), new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build());

        firebaseAuthWithGoogle(GoogleSignIn.getLastSignedInAccount(getApplicationContext()));

        binding.btnGoogle.setOnClickListener(view1 -> signInClient.launch(googleSignInClient.getSignInIntent()));

        binding.btnSignin.setOnClickListener(v -> {
                    /*FirebaseAuth.getInstance()
                            .signInWithEmailAndPassword(
                                    "paul@gmail.com",
                                    "123456"
                            ).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        }
                    });*/
            if (binding.tilEmail.getEditText().getText().toString().trim().isEmpty() || binding.tilPass.getEditText().getText().toString().trim().isEmpty()) {
                Toast.makeText(getApplicationContext(), "Enter the indicated credentials", Toast.LENGTH_SHORT);
            } else {
                FirebaseAuth.getInstance()
                        .signInWithEmailAndPassword(
                                binding.tilEmail.getEditText().getText().toString().trim(),
                                binding.tilPass.getEditText().getText().toString().trim()
                        ).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    } else {
                        Toast.makeText(getApplicationContext(), task.getException().getLocalizedMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        binding.btnSignup.setOnClickListener(v -> {
            /*FirebaseAuth.getInstance()
                    .signInWithEmailAndPassword(
                            "paul2@gmail.com",
                            "123456"
                    ).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
            });*/
            startActivity(new Intent(this, SignupActivity.class));
        });
    }

    ActivityResultLauncher<Intent> signInClient = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    try {
                        firebaseAuthWithGoogle(GoogleSignIn.getSignedInAccountFromIntent(result.getData()).getResult(ApiException.class));
                    } catch (ApiException e) {

                    }
                }
            });

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        if(account == null) return;

        binding.signInProgressBar.setVisibility(View.VISIBLE);
        //binding.signInFrom.setVisibility(View.GONE);

        FirebaseAuth.getInstance().signInWithCredential(GoogleAuthProvider.getCredential(account.getIdToken(), null))
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        db.collection("users").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).get().addOnCompleteListener(task1 -> {
                            PublicUser pUser = new PublicUser();
                            if (task1.getResult().exists()) {
                                pUser = task1.getResult().toObject(PublicUser.class);
                            } else {
                                pUser.userName = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
                                pUser.gamesPlayed = 0;
                                pUser.gamesWon = 0;
                                pUser.points = 1000;
                            }
                            db.collection("users").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).set(pUser);
                        });
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    } else {
                        binding.signInProgressBar.setVisibility(View.GONE);
                    }
                });
    }

}